package corebanking.repositories;

import corebanking.entities.Commission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissionRepository extends JpaRepository<Commission, Integer>{

    Commission findCommissionById(Integer id);
}
