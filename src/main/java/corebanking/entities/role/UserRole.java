package corebanking.entities.role;

public enum UserRole {
    ROLE_JUNIOR,
    ROLE_MIDDLE,
    ROLE_SENIOR
}
