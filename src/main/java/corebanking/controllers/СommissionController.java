package corebanking.controllers;

import corebanking.entities.Commission;
import corebanking.services.CommissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/commission")
@RequiredArgsConstructor
public class СommissionController {

    private final CommissionService commissionService;

    @GetMapping
    public List<Commission> getAll() {
        return commissionService.getAll();
    }

    @GetMapping("/{id}")
    public Commission getById(@PathVariable Integer id) {
        return commissionService.getById(id);
    }

    @PostMapping
    public void save(@RequestBody Commission commission) {
        commissionService.save(commission);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        commissionService.delete(id);
    }
}
