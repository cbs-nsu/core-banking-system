package corebanking.services;

import corebanking.entities.Commission;
import corebanking.repositories.CommissionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommissionService {

    private final CommissionRepository commissionRepository;

    public List<Commission> getAll() {
        return commissionRepository.findAll();
    }

    public Commission getById(Integer id) {
        return commissionRepository.findCommissionById(id);
    }

    public void save(Commission commission) {
        commissionRepository.save(commission);
    }

    public void delete(Integer id) {
        commissionRepository.deleteById(id);
    }
}
