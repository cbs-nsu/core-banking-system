package corebanking.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "commissions")
public class Commission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "interest")
    private Integer interest;
}
