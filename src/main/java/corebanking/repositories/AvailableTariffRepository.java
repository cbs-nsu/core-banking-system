package corebanking.repositories;

import corebanking.entities.AvailableTariff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AvailableTariffRepository extends JpaRepository<AvailableTariff, Integer> {

    AvailableTariff findAvailableTariffById(Integer id);

    List<AvailableTariff> findAllByNumberPassport(Long numberPassport);

    AvailableTariff findByNumberPassportAndIdTariff(Long numberPassport, Integer idTariff);
}
