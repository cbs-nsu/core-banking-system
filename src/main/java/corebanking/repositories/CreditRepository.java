package corebanking.repositories;

import corebanking.entities.Credit;
import corebanking.entities.states.CreditStates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Integer> {

    Credit findCreditById(Integer id);

    Credit findCreditByStateAndIdAvailTariffIn(CreditStates state, List<Integer> ids);

    List<Credit> findAllByStateAndIdAvailTariffIn(CreditStates state, List<Integer> idsAvailTariff);
}
